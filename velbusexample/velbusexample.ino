/* 
VELBUS example
used pins:
  2 (IN)  : used by CAN Shield for interrupt when new CAN message received
  5 (IN)  : when this is high, velbus packets will be decoded and sent to Serial
  6 (IN)  : when low, a scan-packet will be sent to the bus
  7 (OUT) : will be high when an error has occured
  8 (OUT) : will switch state everytime a velbus packet has been received
  10 (OUT): Can Chip Select (used for the CAN shield)
  11, 12, 13 (OUT): used by the CAN shield
  
  pins 2, 10, 11, 12, 13 are used by the CAN shield and cannot be used for other purposes
  pins 5, 6, 7, 8 are only used in this sketch and can be used for other purposes
  
functions to be used for communicating with VELBUS
    INT8U begin(int pin = 10, INT8U speedset = CAN_16KBPS): Call this in setup() to begin connection to VELBUS
    INT8U checkReceive(void)                              : returns CAN_MSGAVAIL when a new packet is available or returns CAN_NOMSG when no new packet is available
    INT8U checkError(void)                                : returns CAN_OK when no error in the CAN controller or returns CAN_CTRLERROR when there is a problem with the CAN controller
    boolean sendpacket(VelbusPacket PacketToSend)         : sends the packet 'PacketToSend' to the bus and returns true upon successfull transmittion. When the time between the previous packet sent is less then 90ms, the packet will not be sent and the function returns false
    boolean receivepacket(VelbusPacket &ReceivedPacket)   : If a new packet is available, 'ReceivedPacket' will be filled with the new packet and true will be returned. If no new packet is available, false will be returned
    boolean cansend (void)                                : returns true when a next packet can be sent. 

*/

#include "velbus.h"
#include <SPI.h>
#include <stdio.h>
#define INT8U unsigned char
#define BufferLength 128

int receiveled = 8;
int errorled = 7;
unsigned long Teller = 0;
int scanbutton = 6;
int buttonenable = 5;
byte addressTeller = 0xFF;
unsigned long buttonDebounce = millis() + 75;

VelbusPacket vbpbuffer[BufferLength];
VelbusPacket receivedpacket;
VelbusPacket sendpacket;
int BufferStart = 0;
int BufferEnd = 0;

void setup()
{
  pinMode(receiveled, OUTPUT);  
  pinMode(scanbutton,INPUT_PULLUP);
  pinMode(buttonenable , INPUT_PULLUP);
  pinMode(errorled,OUTPUT);
  
  digitalWrite(errorled, LOW);
  digitalWrite(receiveled, LOW);

  VELBUS.begin();                   // init velbus : default baudrate = 16k666, default SPI chip select pin = 10
  attachInterrupt(0, MCP2515_ISR, FALLING); // start interrupt when new packet has been received
  Serial.begin(9600);
  Serial.println("Arduino start");
  
  sendpacket.RTR = true;                // prepare velbus packet to send
  sendpacket.Priority = Priority::low;
}

void MCP2515_ISR()
{
     digitalWrite(receiveled, !digitalRead(receiveled));    // switch led on receiving new packet
     
     // EXAMPLE OF RECEIVING PACKET
     while (VELBUS.checkReceive() == CAN_MSGAVAIL)
       {
         if (VELBUS.receivepacket(receivedpacket))
         {
             int BufferEndNow = BufferEnd;
             BufferEndNow++;                                  // fill receivebuffer with new packet so we can process it in the loop
             if (BufferEndNow >= BufferLength) BufferEndNow = 0;
             if (BufferEndNow != BufferStart)
             {
               vbpbuffer[BufferEndNow] = receivedpacket;
               BufferEnd = BufferEndNow;
             };
             Teller ++;
         };
       };
     
}

void loop()
{
    if(BufferStart != BufferEnd)                   // check if buffer is not empty
    {
      BufferStart ++;
      if (BufferStart >= BufferLength) BufferStart = 0;
      
      // below this line you can start processing the packet in the buffer. The eldest packet in the buffer is: vbpbuffer[BufferStart]
      // in our example we will display the data via the serial when pin 5 is low
      // begin example
         if (digitalRead(buttonenable) == HIGH)
         {
              Serial.print("CAN_BUS GET DATA "); Serial.println (Teller);
              // display the packet
              Serial.print("Address: "); Serial.println(vbpbuffer[BufferStart].Address, 16);
              Serial.print("RTR    : "); Serial.println(vbpbuffer[BufferStart].RTR);
              Serial.print("Prio   : "); Serial.println(vbpbuffer[BufferStart].Priority, 2);
              Serial.print  ("Data   : ");
              for(int i = 0; i<vbpbuffer[BufferStart].dataLen; i++)    // print the data
              {
                Serial.print(vbpbuffer[BufferStart].Data[i], 16);Serial.print(" ");
              }
              Serial.println();
              Serial.println();
         };
       // end example
    }
    
    
    // EXAMPLE OF SENDING VELBUS PACKET
    
    // below we check the button on pin 6. When the pin is LOW for 75ms (debounce time of 75ms) we send a scan packet to the bus
    
    if (digitalRead(scanbutton) == LOW)
    {
      if (millis() > buttonDebounce && buttonDebounce > 0)
      {
        addressTeller = (byte) (addressTeller + 1);
        sendpacket.Address = addressTeller;
        if (VELBUS.sendpacket(sendpacket)) digitalWrite(errorled,LOW); else digitalWrite(errorled, HIGH); // if you send a next packet within 90ms after the previous packet, VELBUS.sendpacket() will return false and the packet will not be sent (to avoid flooding the velbus). To avoid this, you can check VELBUS.cansend() before sending the packet
        buttonDebounce = 0;
      };
    }
    else
    {
      buttonDebounce = millis() + 75;
    };
}
